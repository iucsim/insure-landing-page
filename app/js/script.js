const menuBtn = document.querySelector(".menuBtn");
// const closeBtn = document.querySelector(".closeBtn");
const main = document.querySelector(".main");
const menu = document.querySelector(".menu");
const body = document.body;

menuBtn.addEventListener('click', () => {
    if(menuBtn.getAttribute("src") === "/images/icon-hamburger.svg"){
        console.log("menu-button click");
        menuBtn.setAttribute('src', '/images/icon-close.svg');
        main.classList.add("hidden");
        menu.classList.add("flex");
        body.classList.add("height");
       
    }else{
        console.log("close-button click");
        menuBtn.setAttribute('src', '/images/icon-hamburger.svg');
        main.classList.remove("hidden");
        menu.classList.remove("flex");
        body.classList.remove("height");
    }
})

